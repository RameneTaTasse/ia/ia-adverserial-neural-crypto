# TP IA - Adversarial Neural Crypto

Nous nous sommes basés sur un modèle avec 4 couches de convolution à une dimension, modèle trouvé sur internet (papiers).
Nous avons testé des valeurs différentes pour les paramètres epoch et vitesse d'apprentissage. Changer les fonctions d'activation ne rend pas meilleur le résultat obtenu, tout comme le fait d'essayer d'ajouter des layers dense (cela ralentit juste un peu Eve.