#!/usr/bin/python

import tensorflow as tf
import numpy as np
from datetime import datetime

MSG_LEN = 16
KEY_LEN = 16
BATCH_SIZE = 4096
EPOCHS=10000
LEARNING_RATE = 0.0008
ADAM_BETA1 = 0.9
ADAM_EPSILON = 1e-08
FILTERS = [
    [4, 1, 2],
    [2, 2, 4],
    [1, 4, 4],
    [1, 4, 1]
]


log_dir="logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
tf.keras.backend.set_floatx('float64')

def createModelAlice(input_size, neuron_size, output_size):
    alice = tf.keras.models.Sequential([
        tf.keras.layers.Dense(neuron_size, activation="relu", input_shape=(input_size,)),
        tf.keras.layers.Dense(neuron_size, activation="relu"),
        tf.keras.layers.Reshape((neuron_size,1)),
        tf.keras.layers.Conv1D(2, 4, activation='sigmoid', strides=1, padding="same"),
        tf.keras.layers.Conv1D(4, 2, activation='sigmoid', strides=2, padding="valid"),
        tf.keras.layers.Conv1D(4, 1, activation='sigmoid', strides=1, padding="same"),
        tf.keras.layers.Conv1D(1, 1, activation='tanh', strides=1, padding="same"),
        tf.keras.layers.Reshape((output_size,))
    ])
    return alice


def createModels(input_size, neuron_size, output_size):

    model = tf.keras.models.Sequential([
        tf.keras.layers.Dense(neuron_size, activation="relu", input_shape=(input_size,)),
        tf.keras.layers.Reshape((neuron_size,1)),
        tf.keras.layers.Conv1D(2, 4, activation='sigmoid', strides=1, padding="same"),
        tf.keras.layers.Conv1D(4, 2, activation='sigmoid', strides=2, padding="valid"),
        tf.keras.layers.Conv1D(4, 1, activation='sigmoid', strides=1, padding="same"),
        tf.keras.layers.Conv1D(1, 1, activation='tanh', strides=1, padding="same"),
        tf.keras.layers.Reshape((output_size,))
    ])
    return model

def gen_data(n=BATCH_SIZE, msg_len=MSG_LEN, key_len=KEY_LEN):
    return (np.random.uniform(0, 2, size=(msg_len))-1),(np.random.uniform(0, 2, size=(key_len))-1)

msg_in_val, key_val = gen_data(n=BATCH_SIZE, msg_len=MSG_LEN, key_len=KEY_LEN)
# print(msg_in_val.shape)
# print(key_val.shape)


inpt = np.concatenate((np.array([msg_in_val]), np.array([key_val])),axis=1)
# print(inpt.shape)

alice = createModelAlice(MSG_LEN+KEY_LEN, MSG_LEN+KEY_LEN, MSG_LEN)
bob = createModels(MSG_LEN+KEY_LEN, MSG_LEN+KEY_LEN, MSG_LEN)
eve = createModels(MSG_LEN, MSG_LEN+KEY_LEN, MSG_LEN)

#optimizer = tf.keras.optimizers.SGD(learning_rate=LEARNING_RATE)

optimizer = tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE, beta_1=ADAM_BETA1, epsilon = ADAM_EPSILON)
#loss_fn = tf.keras.losses.CategoricalCrossentropy()

ts = str(datetime.now().timestamp())
summary_writer = tf.summary.create_file_writer("logs/train_" + ts)

msg_in_val, key_val = gen_data(n=BATCH_SIZE, msg_len=MSG_LEN, key_len=KEY_LEN)
inputAlice = np.concatenate((np.array([msg_in_val]), np.array([key_val])),axis=1)


for epoch in range(EPOCHS):

    with tf.GradientTape(persistent=True) as tape:
        outAlice = alice(inputAlice)
        # print(outAlice[0])
        inputBob = np.concatenate((np.array([outAlice[0]]), np.array([key_val])),axis=1)
        outBob = bob(inputBob)
        outEve = eve(outAlice)
        #loss = loss_fn(out, msg_in_val)
        # Alice perd quand Eve dechiffre et pas Bob
        # Bob perd quand il dechiffre moins que Eve
        # Eve perd quand elle ne dechiffre pas le message
        lossEve = tf.reduce_mean(tf.abs(outEve - msg_in_val))
        lossBob = tf.reduce_mean(tf.abs(outBob - msg_in_val))
        lossAlice = lossBob + (1. - lossEve)**2
    #   lossAlice = lossBob + tf.reduce_sum( tf.square(float(MSG_LEN) / 2.0 - lossEve) / ((MSG_LEN / 2)**2) )
    gradsAlice = tape.gradient(lossAlice, alice.trainable_variables)
    optimizer.apply_gradients(zip(gradsAlice, alice.trainable_variables))
    gradsBob = tape.gradient(lossBob, bob.trainable_variables)
    optimizer.apply_gradients(zip(gradsBob, bob.trainable_variables))
    gradsEve = tape.gradient(lossEve, eve.trainable_variables)
    optimizer.apply_gradients(zip(gradsEve, eve.trainable_variables))

    with summary_writer.as_default():
        tf.summary.scalar("lossBob", lossBob, step=epoch)
        tf.summary.scalar("lossEve", lossEve, step=epoch)
        tf.summary.scalar("lossAlice", lossAlice, step=epoch)
    if epoch % 100 == 0:
        tf.keras.backend.print_tensor(lossAlice, "Loss Alice : ")
        tf.keras.backend.print_tensor(lossBob, "Loss Bob : ")
        tf.keras.backend.print_tensor(lossEve, "Loss Eve : ")
