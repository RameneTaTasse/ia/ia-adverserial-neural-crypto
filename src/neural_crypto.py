#!/usr/bin/python

import numpy as np
import tensorflow as tf

EPOCHS = 1000

optimizer = tf.keras.optimizers.SGD(learning_rate=0.1)
loss_fn = tf.keras.losses.CategoricalCrossentropy()
# gen data

for epoch in range(EPOCHS):
    with tf.GradientTape() as tape:
        x, y = next(mydata_it)
        out = mynet(x)
        loss = loss_fn(out, x)
    grads = tape.gradient(loss, mynet.trainable_variables)
    optimizer.apply_gradient(zip(grads, mynet.trainable_variables))
