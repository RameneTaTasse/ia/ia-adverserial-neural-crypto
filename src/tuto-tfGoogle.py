#!/usr/bin/python

import tensorflow as tf
import numpy as np
import datetime

mnist = tf.keras.datasets.mnist
EPOCHS = 1000
BATCH_SIZE = 500
MSG_LEN = 16
KEY_LEN = 16

# (x_train, y_train),(x_test, y_test) = mnist.load_data()
# x_train, x_test = x_train / 255.0, x_test / 255.0
# x_train_iter, y_train_iter = np.nditer(x_train), np.nditer(y_train)
# x_iter = np.nditer(x_train)
#
# print(x_train)
# print(next(x_iter))

def gen_data(n=BATCH_SIZE, msg_len=MSG_LEN, key_len=KEY_LEN):
    return (np.random.randint(0, 2, size=(n, msg_len))*2-1), \
           (np.random.randint(0, 2, size=(n, key_len))*2-1)

msg_in_val, key_val = gen_data(n=BATCH_SIZE, msg_len=MSG_LEN, key_len=KEY_LEN)
print(msg_in_val.shape)

msg_iter = np.nditer(msg_in_val)

optimizerr = tf.keras.optimizers.SGD(learning_rate=0.1)
loss_fn = tf.keras.losses.CategoricalCrossentropy()

def create_model():
    return tf.keras.models.Sequential([
        tf.keras.layers.Input((28, 28)), #(32,)
        tf.keras.layers.Dense(512, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(10, activation='softmax')
    ])

model = create_model()

log_dir="logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

# model.fit(x=x_train,
#           y=y_train,
#           epochs=5,
#           validation_data=(x_test, y_test),
#           callbacks=[tensorboard_callback])
#

# batch puis 32 val aleatoires (debut que des 1 pour etudier le comportement)

for epoch in range(EPOCHS):
    with tf.GradientTape() as tape:
        x = next(msg_iter)
        out = model(x)
        loss = loss_fn(out, x)
    grads = tape.gradient(loss, mynet.trainable_variables)
    optimizer.apply_gradient(zip(grads, mynet.trainable_variables))
